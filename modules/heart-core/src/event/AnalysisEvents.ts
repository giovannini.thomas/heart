/**
 * List of events names
 * @see {@link https://www.typescriptlang.org/docs/handbook/enums.html}
 */
const enum AnalysisEvents {
  DONE = 'analysis.done',
}

export default AnalysisEvents;
