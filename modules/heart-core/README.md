# Description

_Heart Core_ centralize helpful code needed by every _Heart_ modules.

THIS PACKAGE DOES NOT NEED TO FIGURE IN YOUR DEPENDENCIES.

Read more about the purpose, design and general installation of _Heart_ on [the dedicated wiki](https://gitlab.com/fabernovel/heart/wikis/What-is-Heart).
