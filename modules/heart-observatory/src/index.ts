import ObservatoryModule from './ObservatoryModule.js';

export default new ObservatoryModule({
  name: 'Heart Observatory',
  service: {
    name: 'Mozilla Observatory',
    logo: 'https://gitlab.com/fabernovel/heart/raw/master/assets/images/logos/Observatory.png?v=20190723'
  },
});
