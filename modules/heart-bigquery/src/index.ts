import BigQueryModule from './BigQueryModule';

export default new BigQueryModule({
  name: 'Heart BigQuery',
  service: {
    name: 'Google BigQuery',
    logo: 'https://gitlab.com/fabernovel/heart/raw/master/assets/images/logos/BigQuery.png'
  },
});
