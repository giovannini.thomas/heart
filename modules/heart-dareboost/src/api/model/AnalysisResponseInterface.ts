/**
 * @see {@link https://www.dareboost.com/fr/documentation-api#analyse}
 */
export default interface AnalysisResponseInterface {
  status: string;
  message: string;
  reportId: string;
}
